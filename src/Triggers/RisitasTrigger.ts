import Discord from "discord.js";
import { ChannelType } from "../Discord/ChannelType";
import { RandomExtension } from "../Utils/RandomExtension";
import { Trigger } from "./Trigger";

export class RisitasTrigger extends Trigger {
  protected trigger: string | RegExp = "aya|issou|risitas|chancla";
  protected channelTypes: ChannelType[] = [ChannelType.TEXT];

  public async process(message: Discord.Message): Promise<boolean> {
    if (RandomExtension.between(1, 10) !== 1) {
      return false;
    }
    const emoji = this.bot.client.emojis.find((e) => e.name === "risitas");
    if (!emoji) {
      return false;
    }
    await message.react(emoji);
    return true;
  }
}
