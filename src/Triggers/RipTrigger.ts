import Discord from "discord.js";
import { ChannelType } from "../Discord/ChannelType";
import { Trigger } from "./Trigger";

export class RipTrigger extends Trigger {
  protected trigger: string | RegExp = "rip|dead|morte?s?";
  protected channelTypes: ChannelType[] = [ChannelType.TEXT, ChannelType.DM, ChannelType.GROUP_DM];

  public async process(message: Discord.Message, oldMessage?: Discord.Message | undefined): Promise<boolean> {
    const emoji = this.bot.client.emojis.find((e) => e.name === "RIP");
    if (!emoji) {
      return false;
    }
    await message.react(emoji);
    return true;
  }
}
