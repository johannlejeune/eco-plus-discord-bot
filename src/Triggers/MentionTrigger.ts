import Discord from "discord.js";
import { ChannelType } from "../Discord/ChannelType";
import { Trigger } from "./Trigger";

export class MentionTrigger extends Trigger {
  protected trigger: string | RegExp = /.*/;
  protected channelTypes: ChannelType[] = [ChannelType.TEXT, ChannelType.DM, ChannelType.GROUP_DM];

  public async process(message: Discord.Message): Promise<boolean> {
    if (message.mentions.users.has(this.bot.client.user.id)) {
      await message.reply("Oui ? :blush:");
      return true;
    }
    return false;
  }
}
