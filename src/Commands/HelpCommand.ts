import Discord from "discord.js";
import { ChannelType } from "../Discord/ChannelType";
import { Command, ICommandOption } from "./Command";

export class HelpCommand extends Command {
  public commands: Map<string, ICommandOption> = new Map([
    ["help", {
      channelTypes: [ChannelType.TEXT, ChannelType.DM, ChannelType.GROUP_DM],
      help: "Envoie ce message. :spy:",
    }],
  ]);
  public deleteOriginalMessage: boolean = true;

  protected async process(command: string, args: string[], message: Discord.Message): Promise<boolean> {
    // Create the list of accessible commands and add the title to it.
    const accessibleCommands: string[] = ["Voici toutes les commandes disponibles.\n"];
    for (const commandClass of this.bot.messageHandler.commands) {
        for (const [commandName, commandOptions] of commandClass.commands) {
          if (commandOptions.channelTypes
              && !commandOptions.channelTypes.includes(message.channel.type as ChannelType)) {
            continue;
          }
          accessibleCommands.push(this.generateHelp(commandName, commandOptions));
        }
    }
    // Send the list.
    const msg = accessibleCommands.join("\n");
    const options = {
      split: true,
    };
    await this.temporaryReplyToMessage(message, msg, options);
    return true;
  }

  private generateHelp(commandName: string, commandOptions: ICommandOption): string {
    let help = `• \`/${commandName}\` `;
    const additionalData = [];
    if (commandOptions.channels) {
      const channels = commandOptions.channels.map((c) => `#${c}`).join(", ");
      additionalData.push(`uniquement dans les canaux ${channels}`);
    }
    if (commandOptions.roles) {
      const roles = commandOptions.roles.join(", ");
      additionalData.push(`uniquement pour les roles ${roles}`);
    }
    if (additionalData.length > 0) {
      help += ` (_${additionalData.join(" ; ")}_) `;
    }
    help += `: ${commandOptions.help || "Devine."}`;
    return help;
  }
}
