import { Bot } from "./Bot";
import { Configuration } from "./Configuration/Configuration";
import { ArgvHelper } from "./Utils/ArgvHelper";
import { Logger } from "./Utils/Logger";

let bot: Bot | undefined;

// Catch exit to provide clean shutdown.
process.once("exit", (code: number) => {
  Logger.getLogger().info("Shutting down...");
  Logger.shutdown().catch((error) => {
    /* tslint:disable no-console */
    console.error("Error while shutting down logger.", error);
    /* tslint:enable no-console */
  });
  if (bot) {
    bot.stop().catch((error) => {
      /* tslint:disable no-console */
      console.error("Error while shutting down bot.", error);
      /* tslint:enable no-console */
    });
  }
  process.exit(code);
});

// Catch uncaught exceptions.
process.on("uncaughtException", (error) => {
  /* tslint:disable no-console */
  console.error("Uncaught exception.", error);
  /* tslint:enable no-console */
  process.exit(1);
});

process.on("SIGINT", () => {
    // Emit the event so that it gets caught once.
    process.emit("exit", 0);
});

async function main() {
  // Parse arguments.
  const argv = ArgvHelper.argv;

  // Load configuration.
  const configuration = await Configuration.loadConfiguration(argv.config);

  // Setup logger.
  await Logger.setupLogger(configuration.logsDirectory ? configuration.logsDirectory : undefined);
  const logger = Logger.getLogger();

  logger.info("All good, starting bot...");

  // Create bot and start it.
  bot = new Bot();
  await bot.run();
}

main().catch((error) => {
  /* tslint:disable no-console */
  console.error("Uncaught exception.", error);
  /* tslint:enable no-console */
});
