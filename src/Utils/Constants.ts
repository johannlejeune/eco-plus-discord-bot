import path from "path";

export class Constants {
  public static DEFAULT_CONFIG_FILE = path.resolve("./config.json");

  public static RISIBANK_API_SEARCH_URL = "https://api.risibank.fr/api/v0/search";
  public static YOUR_MOM_JOKES_URL = "http://www.une-blague.com/blagues-ta-mere.html";
}
